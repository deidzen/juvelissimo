import json
from pygments import highlight
from pygments.lexers.data import JsonLexer
from pygments.formatters.html import HtmlFormatter

from django.contrib import admin
from django.utils.safestring import mark_safe
from juvelissimoapp.models import Category, Product, CartItem, Cart, Order

# Register your models here.

class APIDataAdmin(admin.ModelAdmin):
    readonly_fields = ('data_prettified',)

    def data_prettified(self, instance):
        # Convert the data to sorted, indented JSON
        response = json.dumps(instance.data, sort_keys=True, indent=2, ensure_ascii=False)

        # Truncate the data. Alter as needed
        response = response[:5000]

        # Get the Pygments formatter
        formatter = HtmlFormatter(style='colorful')

        # Highlight the data
        response = highlight(response, JsonLexer(), formatter)

        # Get the stylesheet
        style = "<style>" + formatter.get_style_defs() + "</style><br>"

        # Safe the output
        return mark_safe(style + response)

    data_prettified.short_description = 'data prettified'

        

def make_paid(modeladmin, request, queryset):
    queryset.update(status='Оплачен')
make_paid.short_description= "Пометить как оплаченные"

class OrderAdmin(admin.ModelAdmin):
    list_filter = ['status']
    actions = [make_paid]

admin.site.register(Category)
admin.site.register(Product, APIDataAdmin)
admin.site.register(CartItem)
admin.site.register(Cart)
admin.site.register(Order, OrderAdmin)