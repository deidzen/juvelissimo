from django.apps import AppConfig


class JuvelissimoappConfig(AppConfig):
    name = 'juvelissimoapp'
