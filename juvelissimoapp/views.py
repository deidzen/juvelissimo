from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.contrib.auth import login, authenticate
from decimal import Decimal
from juvelissimoapp.models import Category, Product, CartItem, Cart, Order
from juvelissimoapp.forms import OrderForm, RegistrationForm, LoginForm, ProductForm
from django.contrib.auth.decorators import permission_required
from django.utils.text import slugify
from django.contrib.postgres.fields import JSONField
# Create your views here.


def base_view(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    categories = Category.objects.all()
    products = Product.objects.all()
    is_moderator = False
    print(request.user)
    if request.user.groups.filter(name='Moderators').exists():
        is_moderator = True
    context = {
        'categories' : categories,
        'products' : products,
        'cart' : cart,
        'is_moderator' : is_moderator,
    }
    return render(request, 'base.html', context)


def product_view(request, product_slug):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    is_moderator = False
    if request.user.groups.filter(name='Moderators').exists():
        is_moderator = True
    categories = Category.objects.all()
    product = Product.objects.get(slug=product_slug)
    context = {
        'categories' : categories,
        'product' : product,
        'cart' : cart,
        'is_moderator' : is_moderator,
    }
    return render(request, 'product.html', context)


def category_view(request, category_slug):
    category = Category.objects.get(slug=category_slug)
    categories = Category.objects.all()
    products_of_category = Product.objects.filter(category=category)
    query = request.GET.get('query')
    
    if query:
        products_of_category = products_of_category.filter(data__title__icontains=query)

    context = {
        'category' : category,
        'products_of_category': products_of_category,
        'categories' : categories,
    }
    return render(request, 'category.html', context)


def cart_view(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    categories = Category.objects.all()
    context = {
        'cart' : cart,
        'categories' : categories,
    }
    return render(request, 'cart.html', context)


def add_to_cart_view(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    product_slug = request.GET.get('product_slug')
    product = Product.objects.get(slug=product_slug)
    cart.add_to_cart(product.slug)
    new_cart_total = 0.00
    for item in cart.items.all():
        new_cart_total += float(item.item_total)
    cart.cart_total = new_cart_total
    cart.save()
    return JsonResponse({'cart_total': cart.items.count(), 'cart_total_price': cart.cart_total})


def remove_from_cart_view(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    product_slug = request.GET.get('product_slug')
    product = Product.objects.get(slug=product_slug)
    cart.remove_from_cart(product.slug)
    new_cart_total = 0.00
    for item in cart.items.all():
        new_cart_total += float(item.item_total)
    cart.cart_total = new_cart_total
    cart.save()
    return JsonResponse({'cart_total': cart.items.count(), 'cart_total_price': cart.cart_total})


def change_item_quantity(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    quantity = request.GET.get('quantity')
    item_id = request.GET.get('item_id')
    cart.change_quantity(quantity, item_id)
    cart_item = CartItem.objects.get(id=int(item_id))
    return JsonResponse(
        {'cart_total': cart.items.count(), 
        'item_total': cart_item.item_total,
        'cart_total_price': cart.cart_total})


def checkout_view(request):
    categories = Category.objects.all()
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    context = {
        'cart' : cart,
        'categories' : categories,
    }
    return render(request, 'checkout.html', context)


def order_create_view(request):
    categories = Category.objects.all()
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    form = OrderForm(request.POST or None)
    context = {
        'form' : form,
        'cart' : cart,
        'categories' : categories,
    }
    return render(request, 'order.html', context)


def make_order_view(request):
    categories = Category.objects.all()
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    form = OrderForm(request.POST or None)
    if form.is_valid():
        name = form.cleaned_data['name']
        last_name = form.cleaned_data['last_name']
        phone = form.cleaned_data['phone']
        buying_type = form.cleaned_data['buying_type']
        address = form.cleaned_data['address']
        comments = form.cleaned_data['comments']
        new_order = Order.objects.create(
            user = request.user,
            items = cart,
            total = cart.cart_total,
            first_name = name,
            last_name = last_name,
            phone = phone,
            address = address,
            buying_type = buying_type,
            comments = comments
        )
        del request.session['cart_id']
        del request.session['total']
        return HttpResponseRedirect(reverse('thank_you'))
    return render(request, 'order.html', {'categories' : categories})


def account_view(request):
    categories = Category.objects.all()
    order = Order.objects.filter(user=request.user).order_by('-id')
    context = {
        'order' : order,
        'categories' : categories,
    }
    return render(request, 'account.html', context)


def registration_view(request):
    categories = Category.objects.all()
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        new_user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        email = form.cleaned_data['email']
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        new_user.username = username
        new_user.set_password(password)
        new_user.first_name = first_name
        new_user.last_name = last_name
        new_user.email = email
        new_user.save()
        login_user = authenticate(username=username, password=password)
        if login_user:
            login(request, login_user)
            return HttpResponseRedirect(reverse('base'))
    context = {
        'form' : form,
        'categories' : categories,
    }
    return render(request, 'registration.html', context)


def login_view(request):
    categories = Category.objects.all()
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        login_user = authenticate(username=username, password=password)
        if login_user:
            login(request, login_user)
            return HttpResponseRedirect(reverse('base'))
    context = {
        'form' : form,
        'categories' : categories,
    }
    return render(request, 'login.html', context)

@permission_required('juvelissimoapp.add_product', raise_exception=True)
def add_product_view(request):
    categories = Category.objects.all()
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    form = ProductForm(request.POST or None)
    context = {
        'form' : form,
        'cart' : cart,
        'categories' : categories,
    }
    return render(request, 'add_product.html', context)


def make_product_view(request):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    form = ProductForm(request.POST, request.FILES)
    if form.is_valid():
        category_slug = form.cleaned_data['category']
        category = Category.objects.get(slug=slugify(category_slug))
        title = form.cleaned_data['title']
        description = form.cleaned_data['description']
        image = form.cleaned_data['image']
        price = form.cleaned_data['price']
        available = form.cleaned_data['available']
        new_product = Product.objects.create(
            category = category,
            description = description,
            image = image,
            price = price,
            available = available,
            data = {
                'category' : category_slug,
                'title': title
            }
        )
        return HttpResponseRedirect(reverse('product_is_made'))
    context = {
        'form' : form,
        'categories' : categories,
    }
    return render(request, 'add_product.html', context)

def delete_product_view(request, product_slug):
    try:
        cart_id = request.session['cart_id']
        cart = Cart.objects.get(id=cart_id)
        request.session['total'] = cart.items.count()
    except:
        cart = Cart()
        cart.save()
        cart_id = cart.id
        request.session['cart_id'] = cart_id
        cart = Cart.objects.get(id=cart_id)
    product = Product.objects.get(slug=product_slug)
    category = product.category
    product.delete()
    return HttpResponseRedirect(reverse('base'))
    